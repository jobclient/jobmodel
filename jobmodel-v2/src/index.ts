import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgCoreCoreModule } from '@ngcore/core';
import { NgCoreBaseModule } from '@ngcore/base';

// import { SampleComponent } from './sample.component';
// import { SampleDirective } from './sample.directive';
// import { SamplePipe } from './sample.pipe';
// import { SampleService } from './sample.service';

// export * from './sample.component';
// export * from './sample.directive';
// export * from './sample.pipe';
// export * from './sample.service';

export * from './common/events/jobclient-model-events';
export * from './data/core/posting-status';
export * from './data/core/application-status';
export * from './data/core/job-type';
export * from './data/core/contact-type';
export * from './data/core/contact-info';
export * from './data/base/job-base';
export * from './data/model/job-summary';
export * from './data/model/job-posting';
export * from './data/model/job-application';
export * from './data/model/job-followup';
export * from './data/model/job-memo';
export * from './data/model/job-record';
export * from './data/model/user-preference';
export * from './data/model/user-profile';


@NgModule({
  imports: [
    CommonModule,

    NgCoreCoreModule.forRoot(),
    NgCoreBaseModule.forRoot()
  ],
  declarations: [
    // SampleComponent,
    // SampleDirective,
    // SamplePipe
  ],
  exports: [
    // SampleComponent,
    // SampleDirective,
    // SamplePipe
  ]
})
export class JobclientModelModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: JobclientModelModule,
      // providers: [SampleService]
    };
  }
}
